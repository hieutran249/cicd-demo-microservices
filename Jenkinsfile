pipeline {
    agent any

    tools {
        maven 'maven_3_9_5'
    }

    stages {
        stage('Check changed services') {
            steps {
                checkout scmGit(branches: [[name: '*/main']], userRemoteConfigs: [[url: 'https://gitlab.com/hieutran249/cicd-demo-microservices.git']])
                // get the previous commit
                echo GIT_PREVIOUS_COMMIT
                script {
                    // Get the list of changed files
                    gitChanges = sh(script: "git diff --name-only ${GIT_PREVIOUS_COMMIT}..origin/main", returnStdout: true).trim().split('\n')

                    // Extract unique service directories from changed files
                    serviceDirectories = gitChanges.collect { file ->
                        // Extract the service directory name
                        def match = file =~ /^(.+?)\//
                        match ? match[0][1] : null
                    }.findAll { it != null }.unique()

                    echo "Changed files: ${gitChanges}"
                    echo "Changed directories: ${serviceDirectories}"
                }
            }
        }

        stage('Build and Push images') {
            steps {
                script {
                    // Build image if the services have any changes
                    if (serviceDirectories.size() > 0) {
                        // Build jar file
                        sh 'mvn clean install'
                        // Loop through each service directory
                        serviceDirectories.each { serviceDir ->
                            // Build the Docker image for the service
                            sh "docker build -t hieutran249/${serviceDir}:${BUILD_NUMBER} ${serviceDir}"
                            // Push the Docker image to the registry
                            withCredentials([usernamePassword(credentialsId: '5a41762c-6524-4789-9ad7-2b8911036210', usernameVariable: 'DOCKER_USERNAME', passwordVariable: 'DOCKER_PASSWORD')]) {
                                sh "docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}"
                                sh "docker push hieutran249/${serviceDir}:${BUILD_NUMBER}"
                            }
                        }
                    }
                }
            }
        }
    }
}
